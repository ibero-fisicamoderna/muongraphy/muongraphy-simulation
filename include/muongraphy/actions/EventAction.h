#ifndef EventAction_h
#define EventAction_h

#include "G4UserEventAction.hh"
#include "globals.hh"

class RunAction;

class AnalysisManager;

class EventAction : public G4UserEventAction {
public:
    EventAction(RunAction *, AnalysisManager *);

    virtual ~EventAction();

public:
    void BeginOfEventAction(const G4Event *);

    void EndOfEventAction(const G4Event *);

private:
    RunAction *fRunAct;
    int fRPCTimePositionCollectionID = -1;

    AnalysisManager *fAnalysisManager;

    G4int fPrintModulo;

};

#endif
