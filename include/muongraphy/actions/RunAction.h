#ifndef RunAction_h
#define RunAction_h

#include "G4UserRunAction.hh"
#include "globals.hh"

//#include "G4Run.hh"

class G4Run;

class AnalysisManager;

class RunAction : public G4UserRunAction {
public:
    RunAction(AnalysisManager *);

    virtual ~RunAction();

    // virtual G4Run* GenerateRun();
    virtual void BeginOfRunAction(const G4Run *);

    virtual void EndOfRunAction(const G4Run *);

    void FillPerEvent(G4double, G4double, G4double, G4double);

private:
    AnalysisManager *fAnalysisManager;

    G4double fSumEAbs, fSum2EAbs;
    G4double fSumEGap, fSum2EGap;

    G4double fSumLAbs, fSum2LAbs;
    G4double fSumLGap, fSum2LGap;
};

#endif
