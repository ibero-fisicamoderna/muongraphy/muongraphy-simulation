#ifndef SourceAngDistribution_h
#define SourceAngDistribution_h

namespace utils {
    double f(const double &x, const double &z);

    double df(const double &x);

    double nextCosSquaredTheta();
}

#endif
