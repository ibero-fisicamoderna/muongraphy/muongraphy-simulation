#ifndef RPCTelescopeSD_h
#define RPCTelescopeSD_h

#include <G4VSensitiveDetector.hh>

#include "RPCTelescopeHit.h"

class RPCTelescopeSD : public G4VSensitiveDetector {
public:
    RPCTelescopeSD(const G4String& name, const std::map<std::string, int> &pvToStation);

    void Initialize(G4HCofThisEvent *) override;

protected:
    G4bool ProcessHits(G4Step *aStep, G4TouchableHistory *ROhist) override;

private:
    // HIT COLLECTIONS
    TimePositionHitsCollection *fHitsCollection{nullptr};
    G4int fHitsCollectionId{-1};

    // STATIONS
    std::map<std::string, int> fPvToStation;
};

#endif
