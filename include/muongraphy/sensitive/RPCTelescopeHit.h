#ifndef TimePositionHit_h
#define TimePositionHit_h

#include <G4VHit.hh>
#include <G4THitsMap.hh>
#include <G4ThreeVector.hh>

/**
  * Custom hit class used in task 4d.
  *
  * It holds infomation about energy deposits and position/time when
  * traversing a layer.
  */
class RPCTelescopeHit : public G4VHit {
public:
    // Memory allocation and de-allocation
    inline void *operator new(size_t);

    inline void operator delete(void *);

    int GetStation() const { return fStation; }

    void SetStation(int station) { fStation = station; }

    G4double GetTime() const { return fTime; }

    void SetTime(G4double time) { fTime = time; }

    G4ThreeVector GetPosition() const { return fPosition; }

    void SetPosition(G4ThreeVector pos) { fPosition = pos; }

private:
    int fStation;
    G4double fTime;
    G4ThreeVector fPosition;
};

using TimePositionHitsCollection = G4THitsCollection<RPCTelescopeHit>;

extern G4ThreadLocal G4Allocator<RPCTelescopeHit> *hitAllocator;

inline void *RPCTelescopeHit::operator new(size_t) {
    if (!hitAllocator) {
        hitAllocator = new G4Allocator<RPCTelescopeHit>;
    }

    return hitAllocator->MallocSingle();
}

inline void RPCTelescopeHit::operator delete(void *aHit) {
    if (!hitAllocator) {
        hitAllocator = new G4Allocator<RPCTelescopeHit>;
    }

    hitAllocator->FreeSingle((RPCTelescopeHit *) aHit);
}

#endif
