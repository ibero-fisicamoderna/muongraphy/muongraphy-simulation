#ifndef AnalysisManager_h
#define AnalysisManager_h

#include "globals.hh"
#include <vector>


class TFile;

class TTree;

class TH1D;

const G4int kMaxHisto = 4;


class AnalysisManager {
public:
    AnalysisManager();

    ~AnalysisManager();

    void StartRun();

    void EndRun();

    void StartEvent();

    void
    AddHit(const int &station, const G4double &time, const G4double &posX, const G4double &posY, const G4double &posZ);

    void EndEvent();

private:
    TFile *fRootFile;

    // NTUPLES
    TTree *fMetadataNtuple;

    int fNEvents;

    // NTUPLE DE EVENTOS
    TTree *fEventsNtuple;

    int fNHits;
    std::vector<int> fHitStation;
    std::vector<double> fHitTime;
    std::vector<double> fHitPosX;
    std::vector<double> fHitPosY;
    std::vector<double> fHitPosZ;
};


#endif
