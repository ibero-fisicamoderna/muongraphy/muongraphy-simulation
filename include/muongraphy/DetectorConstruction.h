#ifndef DetectorConstruction_h
#define DetectorConstruction_h

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

class G4VPhysicalVolume;

class G4LogicalVolume;

/// Detector construction class to define materials and geometry.

class DetectorConstruction : public G4VUserDetectorConstruction {
public:
    // Constructor
    DetectorConstruction();

    // Destructor
    virtual ~DetectorConstruction();

    // Defines the detector geometry and returns a pointer to the physical World Volume
    G4VPhysicalVolume *Construct() override;

    // Register some of the detector's volumes as "sensitive"
    // Add the following, if you have fields / sensitive detectors
    void ConstructSDandField() override;

private:
    std::map<std::string, int> fPvToStation;

};

#endif

