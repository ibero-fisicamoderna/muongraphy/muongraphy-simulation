#include <G4SDManager.hh>
#include "muongraphy/actions/EventAction.h"
#include "muongraphy/AnalysisManager.h"
#include "muongraphy/actions/RunAction.h"
#include "G4ios.hh"
#include "G4Event.hh"
#include <muongraphy/sensitive/RPCTelescopeHit.h>
#include <G4SystemOfUnits.hh>

EventAction::EventAction(RunAction *run, AnalysisManager *histo)
        : G4UserEventAction(), fRunAct(run), fAnalysisManager(histo) {
    fPrintModulo = 100;
}


EventAction::~EventAction() {
}


void EventAction::BeginOfEventAction(const G4Event *evt) {
    G4int evtNb = evt->GetEventID();

    if (evtNb % fPrintModulo == 0)
        G4cout << "\n---> Begin of event: " << evtNb << G4endl;

    // LE DECIMOS A NUESTRO ANALIZADOR QUE EMPIECE UN NUEVO EVENTO
    // BASICAMENTE RESETEA TODAS LAS VARIABLES
    fAnalysisManager->StartEvent();
}


void EventAction::EndOfEventAction(const G4Event *evt) {
    // PEDIMOS EL SENSITIVE DETECTOR MANAGER
    G4SDManager *sdm = G4SDManager::GetSDMpointer();

    // VERIFICAMOS QUE HAYA HABIDO UNA COLLISION
    if (fRPCTimePositionCollectionID < 0) {
        // DATE CUENTA OLENIN. ES EL NOMBRE DEL DETECTOR Y EL NOMBRE DEL PRIMITIVO
        // SI ALGO LE PASO A NUESTRO DETECTOR, ESTA LINEA NOS DIRA CUAL ES LA COLLECCION DE HITS QUE LE CORRESPONDEN
        fRPCTimePositionCollectionID = sdm->GetCollectionID("rpc/TimePosition");
    }

    // VAMOS A OBTENER EL HIT COLLECTION DEL EVENTO
    G4HCofThisEvent *hce = evt->GetHCofThisEvent();

    if (!hce) {
        return;
    }

    // VAMOS A OBTENER NUESTRO TIME POSITION HITS COLLECTION
    TimePositionHitsCollection *timePositionHitsCollection = dynamic_cast<TimePositionHitsCollection *>(
            hce->GetHC(fRPCTimePositionCollectionID)
    );

    if (timePositionHitsCollection) {
        for (auto hit: *timePositionHitsCollection->GetVector()) {
            fAnalysisManager->AddHit(
                    hit->GetStation(),
                    hit->GetTime() / ns,
                    hit->GetPosition().getX() / cm,
                    hit->GetPosition().getY() / cm,
                    hit->GetPosition().getZ() / cm
            );
        }
    }

    // CERRAMOS EL EVENTO
    // AQUI ES CUANDO TOMA LA "FOTO" DE LO QUE HEMOS HECHO DESDE QUE SE LLAMO START EVENT
    fAnalysisManager->EndEvent();
}
