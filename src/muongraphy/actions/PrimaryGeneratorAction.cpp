#include <stdlib.h>

#include <G4Event.hh>
#include <G4Gamma.hh>
#include <G4GeneralParticleSource.hh>
#include <G4ParticleTable.hh>
#include <G4SystemOfUnits.hh>

#include "muongraphy/actions/PrimaryGeneratorAction.h"
#include "muongraphy/utils/SourceAngDistribution.h"

PrimaryGeneratorAction::PrimaryGeneratorAction()
        : G4VUserPrimaryGeneratorAction() {
    gps = new G4GeneralParticleSource();

    // COMMENTAR TODO ESTO SI QUIEREN SOLO USARLO CON LA MACRO SIN EL COS2
    // DESDE AQUI
    G4ParticleDefinition *particle;
    particle = G4ParticleTable::GetParticleTable()->FindParticle("mu-");

    // G4ParticleDefinition* particle = G4Gamma::Definition();
    gps->SetParticleDefinition(particle);

    // Our position will be a point because we want to specify the angles
    gps->GetCurrentSource()->GetPosDist()->SetPosDisType("Point");

    // We will assume all particles are mono energetic
    gps->GetCurrentSource()->GetEneDist()->SetEnergyDisType("Mono");
    gps->GetCurrentSource()->GetEneDist()->SetMonoEnergy(22 * MeV);

    // We will focus are beam at the center of the bottom scintillator
    gps->GetCurrentSource()->GetAngDist()->SetAngDistType("focused");
    // HASTA AQUI
}

PrimaryGeneratorAction::~PrimaryGeneratorAction() {
    delete gps;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event *event) {
    // COMMENTAR TODO ESTO SI QUIEREN SOLO USARLO CON LA MACRO SIN EL COS2
    // DESDE AQUI
    const unsigned int N = (unsigned int) (((double) rand()) / RAND_MAX) * 9 + 1;

    for (unsigned int i = 0; i < N; i++) {
        GeneratePrimary(event);
    }
    // HASTA AQUI

    // DESCOMENTAR LA LINEA DE ABAJO PARA SOLO USAR MACROS
    // gps->GeneratePrimaryVertex(event);
}

void PrimaryGeneratorAction::GeneratePrimary(G4Event *event) {
    double R = 5. * cm; // RADIO QUE ABARCARA LA DISTRIBUCION

    double r_a = (((double) rand()) / RAND_MAX) * R;
    double theta_a = (((double) rand()) / RAND_MAX) * 2 * M_PI;

    double x0 = cos(theta_a) * r_a;
    double y0 = sin(theta_a) * r_a;
    double z0 = -10 * cm; // A QUE ALTURA QUIERO QUE CAIGAN LOS MUONES (PREFERENTMENTE ES LA ALTURA DEL SUELO)

    // SAFE THETA
    double theta = 0;

    while (theta == 0) {
        theta = utils::nextCosSquaredTheta();
    }

    // RANDOM PHI
    double phi = (((double) rand()) / RAND_MAX) * 2 * M_PI;

    G4double z_plane = 200. * cm; // DE QUE ALTURA QUIERO QUE SE GENEREN
    G4double r = z_plane / cos(theta);

    G4double x = x0 + r * sin(theta) * cos(phi);
    G4double y = y0 + r * sin(theta) * sin(phi);
    G4double z = z0 + z_plane;

    gps->GetCurrentSource()->GetPosDist()->SetCentreCoords({x, z, y});
    gps->GetCurrentSource()->GetAngDist()->SetFocusPoint({x0, z0, y0});
    gps->GeneratePrimaryVertex(event);
}
