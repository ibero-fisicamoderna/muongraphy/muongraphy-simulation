#include "muongraphy/actions/ActionInitialization.h"
#include "muongraphy/AnalysisManager.h"
#include "muongraphy/actions/EventAction.h"
#include "muongraphy/actions/PrimaryGeneratorAction.h"
#include "muongraphy/actions/RunAction.h"

#include "G4GeneralParticleSource.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"

// Constructor

ActionInitialization::ActionInitialization() : G4VUserActionInitialization() {
}

// Destructor
ActionInitialization::~ActionInitialization() {
}

void ActionInitialization::BuildForMaster() const {

    // Histo manager
    AnalysisManager *histo = new AnalysisManager();

    SetUserAction(new RunAction(histo));
}

void ActionInitialization::Build() const {

    // Histo manager
    AnalysisManager *histo = new AnalysisManager();

    SetUserAction(new PrimaryGeneratorAction());

    RunAction *runAction = new RunAction(histo);
    SetUserAction(runAction);

    EventAction *eventAction = new EventAction(runAction, histo);
    SetUserAction(eventAction);
}


