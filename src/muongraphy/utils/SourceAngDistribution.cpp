#include <cmath>
#include <cstdlib>

#include "muongraphy/utils/SourceAngDistribution.h"

#define MIN_X 0
#define MAX_X M_PI/2
#define MAX_TOL 1e-6

double utils::f(const double &x, const double &z) {
    return (2 * x + sin(2 * x)) / M_PI - z;
}

double utils::df(const double &x) {
    return (1 + cos(2 * x)) * 2 / M_PI;
}

double utils::nextCosSquaredTheta() {
    double z = ((double) rand()) / RAND_MAX;

    double x = 0;
    double v = utils::f(x, z);
    double dv;

    while (std::abs(v) > MAX_TOL && MIN_X <= x && x <= MAX_X) {
        v = utils::f(x, z);
        dv = utils::df(x);

        if (std::abs(dv) <= MAX_TOL) {
            break;
        }

        x = x - v / dv;
    }

    return x;
}
