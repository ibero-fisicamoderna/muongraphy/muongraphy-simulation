#include <TFile.h>
#include <TH1D.h>
#include <TTree.h>

#include "G4ios.hh"
#include "muongraphy/AnalysisManager.h"


AnalysisManager::AnalysisManager() {
    // ROOT FILE
    fRootFile = 0;

    // METADATA NTUPLE
    fMetadataNtuple = 0;

    fNEvents = 0;

    // EVENT NTUPLE
    fEventsNtuple = 0;

    fNHits = 0;
}

AnalysisManager::~AnalysisManager() {
    if (fRootFile)
        delete fRootFile;
}

void AnalysisManager::StartRun() {
    // Creating a tree container to handle histograms and ntuples.
    // This tree is associated to an output file.
    //
    G4String fileName = "Volcano.root";
    fRootFile = new TFile(fileName, "RECREATE");

    if (!fRootFile) {
        G4cout << " HistoManager::StartRun :"
               << " problem creating the ROOT TFile " << G4endl;
        return;
    }

    // METADATA NTUPLE
    fMetadataNtuple = new TTree("metadata", "Metadata");
    fMetadataNtuple->Branch("nEvents", &fNEvents);

    // EVENT NTUPLE
    fEventsNtuple = new TTree("events", "Events");
    fEventsNtuple->Branch("nHits", &fNHits);
    fEventsNtuple->Branch("hit_station", &fHitStation);
    fEventsNtuple->Branch("hit_time", &fHitTime);
    fEventsNtuple->Branch("hit_posX", &fHitPosX);
    fEventsNtuple->Branch("hit_posY", &fHitPosY);
    fEventsNtuple->Branch("hit_posZ", &fHitPosZ);

    G4cout << "\n----> Output file is open in " << fileName << G4endl;
}

void AnalysisManager::EndRun() {
    // GUARDA EL METADATA NTUPLE
    fMetadataNtuple->Fill();

    // CIERRA EL ROOT FILE
    if (!fRootFile)
        return;

    fRootFile->Write(); // Writing the histograms to the file
    fRootFile->Close(); // and closing the tree (and the file)

    G4cout << "\n----> Histograms and ntuples are saved\n" << G4endl;
    G4cout << "\n----> Olenin es gay!!!!!!!!\n" << G4endl;
}

void AnalysisManager::StartEvent() {
    // INCREMENTA EL NUMERO DE EVENTOS
    fNEvents += 1;

    // RESETEA EL EVENTO
    fNHits = 0;
    fHitStation.clear();
    fHitTime.clear();
    fHitPosX.clear();
    fHitPosY.clear();
    fHitPosZ.clear();
}

void AnalysisManager::AddHit(const int &station, const double &time, const double &posX, const double &posY,
                             const double &posZ) {
    // NTUPLE STUFF
    fNHits += 1;
    fHitStation.push_back(station);
    fHitTime.push_back(time);
    fHitPosX.push_back(posX);
    fHitPosY.push_back(posY);
    fHitPosZ.push_back(posZ);
}

void AnalysisManager::EndEvent() {
    fEventsNtuple->Fill();
}


