// RPC detector

#include "muongraphy/DetectorConstruction.h"
#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Tubs.hh"

#include "G4Transform3D.hh"
#include "G4SubtractionSolid.hh"
#include "G4Polyline.hh"


#include "G4SystemOfUnits.hh"
#include <G4VisAttributes.hh>
#include <G4Colour.hh>

#include <G4AssemblyVolume.hh>

#include <muongraphy/sensitive/RPCTelescopeSD.h>
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4GenericMessenger.hh"
#include "G4SDManager.hh"

DetectorConstruction::DetectorConstruction()
//DetectorConstruction::DetectorConstruction(std::string str_demfile)
        : G4VUserDetectorConstruction() {
    // Read DEMFILE
    //ReadDemFile();
    //ReadDemFile(str_demfile);

}
//          fScoringVolume(0) {};

DetectorConstruction::~DetectorConstruction() {}

/*
// read DEM file
 void DetectorConstruction::ReadDemFile(std::string str_demfile){

  std::cout << "start reading file: " << str_demfile << std::endl;
  std::ifstream fin(str_demfile.c_str());
  if (!fin) {
    std::cerr << "filename " << str_demfile.c_str() << " could not be opened." << std::endl;
    exit(1);
  }
  fin >> fMeshX >> fMeshY;
  fin >> fNumX >> fNumY;
  fDemZ = new double* [fNumX];
  for (int ix = 0; ix < fNumX; ix++) fDemZ[ix] = new double [fNumY];
  double demZMax = -9999;
  double demZMin = 0;
  for (int iy = 0; iy < fNumY; iy++){
    for (int ix = 0; ix < fNumX; ix++){
      fin >> fDemZ[ix][iy];
      demZMax = (fDemZ[ix][iy] > demZMax ? fDemZ[ix][iy] : demZMax);
    }
  }
  fDemZMax = demZMax;
  fDemZMin = demZMin;

  fDemSizeX = fMeshX * double(fNumX);
  fDemSizeY = fMeshY * double(fNumY);
  fDemSizeZ = fDemZMax-fDemZMin;
  fDemZCenter = (fDemZMax + fDemZMin) / 2.0;
  fOffsetX = - fDemSizeX / 2.0;
  fOffsetY = - fDemSizeY / 2.0;
  fOffsetZ = - fDemSizeZ / 2.0;
  std::cout << fMeshX << " " << fMeshY << std::endl;

}*/

/**************************************************************************
********************************* Código **********************************
**************************************************************************/

G4VPhysicalVolume *DetectorConstruction::Construct() {
    G4NistManager *nist = G4NistManager::Instance();
    G4RotationMatrix *rotateMatrixDet = new G4RotationMatrix();
    rotateMatrixDet->rotateX(15. * deg);

/*-------------------------------------------------------------------------
---------------- Dimensiones y posicion del detector ----------------------
-------------------------------------------------------------------------*/
    G4double World_size_x = 400 * cm;
    G4double Pos_x = 0.0 * m;
    G4double World_size_y = 300 * cm;
    G4double Pos_y = 0.0 * m;
    G4double World_size_z = 400 * cm;
    G4double Pos_z = 0.0 * m;

/*-------------------------------------------------------------------------
------------------------ Material en el detector --------------------------
-------------------------------------------------------------------------*/

    G4Material *Air = nist->FindOrBuildMaterial("G4_AIR");
    G4Material *Alu = nist->FindOrBuildMaterial("G4_Al");
    //G4Material* terrainMaterial = G4Material::GetMaterial("StandardRock");

/***************************************************************************
*************************** Creación del Mundo *****************************
***************************************************************************/
    G4Box *WorldBox = new G4Box(
            "World",       // Nombre
            World_size_x,
            World_size_y,       // Tamaños
            World_size_z
    );

    G4LogicalVolume *logicWorld = new G4LogicalVolume(
            WorldBox,  // Sólido
            Air,       // Material
            "World"// Nombre
    );

    G4VPhysicalVolume *World = new G4PVPlacement(
            0,                                      // No rota
            G4ThreeVector(Pos_x, Pos_y, Pos_z),      // Posición
            logicWorld,                               // Volumen logíco
            "RPC-World",                            // Nombre
            0,                                      // Volumen madre
            0,                                  // No operación booleana
            0// Número de copias
    );

/*-------------------------------------------------------------------------
------------------------ Material en el detector --------------------------
-------------------------------------------------------------------------*/
    G4double z, a;
    G4String Simb, NomEle;

    G4Element *elC = new G4Element(NomEle = "Carbon", Simb = "C", z = 6., a = 12.0107 * g / mole);
    G4Element *elH = new G4Element(NomEle = "Hydrogen", Simb = "H", z = 1., a = 1 * g / mole);
    G4Element *elO = new G4Element(NomEle = "Oxygen", Simb = "O", z = 8., a = 16 * g / mole);

/***************************************************************************
*************************** Creación de la baquelita ***********************
***************************************************************************/

    G4String NomMat;
    G4int nCom, nAt;
    G4double Den, co2Por, FractMass;

    Den = 1.4 * g / cm3;
    G4Material *Bakelite = new G4Material(NomMat = "Bakelite", Den, nCom = 3);

    Den = 1.977 * kg / m3;
    G4Material *CO2 = new G4Material(NomMat = "CarbonDioxide", Den, nCom = 2);

    CO2->AddElement(elC, nAt = 1);
    CO2->AddElement(elO, nAt = 2);

    Bakelite->AddElement(elC, nAt = 1);
    Bakelite->AddElement(elH, nAt = 4);
    Bakelite->AddElement(elO, nAt = 2);

    G4Material *Ar = nist->FindOrBuildMaterial("G4_Ar");
    co2Por = 30; // Porcentaje de la mezcla de CO2
    Den = ((3994.8 + 4.062 * co2Por) / 2240) * kg / m3; //Densidad de la mezcla

    G4Material *Gas = new G4Material(NomMat = "ArCO2Mix", Den, nCom = 2);
    Gas->AddMaterial(CO2, FractMass = co2Por * perCent);
    Gas->AddMaterial(Ar, FractMass = (100 - co2Por) * perCent);


/***************************************************************************
****************************** Placa de Baquelita **************************
***************************************************************************/

    G4double Grosor;
    Grosor = 10;
    G4double Cor = 50. * mm;
    //G4double PosDet = (Grosor / 2 + 1) * mm, Cor = 50 * mm;
    G4double L1 = 7.5 * cm;
    G4double L2 = 7.5 * cm;
    G4double Wth = 1 * mm;
    G4double Gr = 2 * mm;

//------------------------------------------------------------------------------
    G4Box *TopBake = new G4Box(
            "TopBake",
            L1,
            L2,
            Wth);

    G4LogicalVolume *TopLog = new G4LogicalVolume(
            TopBake,
            Bakelite,
            "TopLog"
    );

    G4VisAttributes *TopColor = new G4VisAttributes(G4Colour(1.0, 0, 0));
    TopColor->SetVisibility(true);
    TopColor->SetForceSolid(false);
    TopLog->SetVisAttributes(TopColor);

//------------------------------------------------------------------------------
    G4Box *Cavity = new G4Box(
            "Cavity",
            L1,
            L2,
            Wth);

    G4LogicalVolume *CavityLog = new G4LogicalVolume(
            Cavity,
            Gas,
            "CavityLog");

    G4VisAttributes *CavityColor = new G4VisAttributes(G4Colour(1.0, 1.0, 1.0));
    CavityColor->SetVisibility(true);
    CavityColor->SetForceSolid(false);
    CavityLog->SetVisAttributes(CavityColor);

//-------------------------------------------------------------------------------
    G4Box *BotBake = new G4Box(
            "BotBake",
            L1,
            L2,
            Wth);

    G4LogicalVolume *BotLog = new G4LogicalVolume(
            BotBake,
            Bakelite,
            "BotLog");

    G4VisAttributes *BotColor = new G4VisAttributes(G4Colour(0, 1.0, 0));
    BotColor->SetVisibility(true);
    BotColor->SetForceSolid(false);
    BotLog->SetVisAttributes(BotColor);

/***************************************************************************
******************************* Caja contenedora ***************************
***************************************************************************/

    G4Box *Box = new G4Box(
            "Box",       // Nombre
            8.5 * cm,
            8.5 * cm,       // Tamaños
            8.9 * cm
    );

    G4Box *Box1 = new G4Box(
            "Box",       // Nombre
            8.3 * cm,
            8.3 * cm,       // Tamaños
            8.7 * cm
    );

    G4VSolid *BoxHoll = new G4SubtractionSolid(
            "BoxHollow",
            Box,
            Box1,
            0,
            G4ThreeVector(0, 0, 0)
    );

    G4LogicalVolume *BoxHoLog = new G4LogicalVolume(
            BoxHoll,
            Alu,
            "BoxHoLog",
            0,
            0,
            0
    );

    G4VisAttributes *BoxColor = new G4VisAttributes(G4Colour(1.0, 1.0, 0));
    BoxColor->SetVisibility(true);
    BoxColor->SetForceSolid(false);
    BoxHoLog->SetVisAttributes(BoxColor);

/*******************************************************************************
********************************************************************************
***************++********* Ensamblar detectores y caja  ++**********************
********************************************************************************
*******************************************************************************/
    // Define one layer as one assembly volume
    G4AssemblyVolume *assemblyDetector = new G4AssemblyVolume();

    // LAYERS
    int layers = 4;

    // Now instantiate the layers
    int assemblyPVId = -1;
    int assemblyId = assemblyDetector->GetAssemblyID();
    int assemblyImprintId = assemblyDetector->GetImprintsCount() + 1;

    for (int i = 0; i < layers; i++) {
        G4RotationMatrix Rtop;
        G4ThreeVector Ttop;

        Ttop.setZ(-8 * cm + i * Cor);

        G4Transform3D Top(Rtop, Ttop);
        assemblyDetector->AddPlacedVolume(TopLog, Top);
        fPvToStation.insert({
                                    "av_" + std::to_string(assemblyId) +
                                    "_impr_" + std::to_string(assemblyImprintId) +
                                    "_TopLog_pv_" + std::to_string(++assemblyPVId),
                                    i + 1
                            });
        TopLog->SetVisAttributes(TopColor);

        for (int j = 0; j < layers; j++) {
            G4RotationMatrix Rcav;
            G4ThreeVector Tcav;

            Tcav.setZ(-8 * cm + Gr + i * Cor);

            G4Transform3D Cav(Rcav, Tcav);
            assemblyDetector->AddPlacedVolume(CavityLog, Cav);
            assemblyPVId++;
            CavityLog->SetVisAttributes(CavityColor);
        }
        for (int k = 0; k < layers; k++) {
            G4RotationMatrix Rbot;
            G4ThreeVector Tbot;

            Tbot.setZ(-8 * cm + 2 * Gr + i * Cor);

            G4Transform3D Bot(Rbot, Tbot);
            assemblyDetector->AddPlacedVolume(BotLog, Bot);
            assemblyPVId++;
        }
    }

    G4RotationMatrix Rm;
    G4ThreeVector Tm;
    G4Transform3D As2(Rm, Tm);
    assemblyDetector->AddPlacedVolume(BoxHoLog, As2);
    BoxHoLog->SetVisAttributes(BoxColor);

    G4RotationMatrix Rn;
    G4ThreeVector Tn;
    Rn = rotateMatrixDet->rotateX(-45. * deg);

    G4Transform3D As3(Rn, Tn);
    assemblyDetector->MakeImprint(logicWorld, As3);

    return World;
}

// Implement the following only if you have fields / sensitive detector
void DetectorConstruction::ConstructSDandField() {
    // OBTENER SENSITIVE DETECTOR
    G4SDManager *sd_manager = G4SDManager::GetSDMpointer();
    sd_manager->SetVerboseLevel(0);

    // CREAMOS UN DETECTOR
    RPCTelescopeSD *rpc_detector = new RPCTelescopeSD("rpc", fPvToStation);

    // ASIGNAMOS EL DETECTOR AL TOPLOG
    SetSensitiveDetector("TopLog", rpc_detector);

    // REGISTRAMOS EL DETECTOR
    sd_manager->AddNewDetector(rpc_detector);
}

