#include "muongraphy/sensitive/RPCTelescopeSD.h"

#include <G4SDManager.hh>

RPCTelescopeSD::RPCTelescopeSD(const G4String &name, const std::map<std::string, int> &pvToStation) :
        G4VSensitiveDetector(name) {
    collectionName.insert("TimePosition");
    fPvToStation = pvToStation;
}

G4bool RPCTelescopeSD::ProcessHits(G4Step *aStep, G4TouchableHistory *ROhist) {
    if (aStep->GetPreStepPoint()->GetCharge() == 0) {
        return false;
    }

    RPCTelescopeHit *hit = new RPCTelescopeHit();

    std::string physicalvolume = aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName();

    hit->SetStation(fPvToStation.at(physicalvolume));
    hit->SetTime(aStep->GetPreStepPoint()->GetGlobalTime());
    hit->SetPosition(aStep->GetPreStepPoint()->GetPosition());

    fHitsCollection->insert(hit);

    // Return
    return true;
}

void RPCTelescopeSD::Initialize(G4HCofThisEvent *hcof) {
    fHitsCollection = new TimePositionHitsCollection(SensitiveDetectorName, collectionName[0]);

    if (fHitsCollectionId < 0) {
        fHitsCollectionId = G4SDManager::GetSDMpointer()->GetCollectionID(GetName() + "/" + collectionName[0]);
    }

    hcof->AddHitsCollection(fHitsCollectionId, fHitsCollection);
}
